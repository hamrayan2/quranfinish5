import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qoranios/Activity/main_act.dart';
import 'package:qoranios/Activity/surelist_act.dart';
import 'package:qoranios/Activity/video_act.dart';
import 'package:qoranios/Classes/database_helper%20.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:url_launcher/url_launcher.dart';

class list_act extends StatefulWidget
{
  DictionaryDataBaseHelper db;
  var item;
  int page;//0=[qoran,maqamat,talavat,amuzesh]     1=[bayat,rast,nahavand,hejaz,segah,chahargah,saba]   2=[talavat1,talavat2]     3=[tajvid,tafkik,rukhani,ravankhani]    4=[namha,khatm,faliyat]
  list_act(var item, DictionaryDataBaseHelper db,int page)
  {
    this.item=item;
    this.db = db;
    this.page=page;
    print("lang=${main_act.lang}");
  }
  @override
  _list_actState createState() => _list_actState();
}

class _list_actState extends State<list_act> {
  //var item = ["قرآن کریم", "تلاوت مجلسی", "آموزش", "متفرقه", "ارتباط با ما"];
  //var item = ["قرآن کریم", "تلاوت مجلسی"];
  FToast fToast;

  String qarykabir="";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);
    fToast = FToast();
    fToast.init(context);
    if(main_act.lang==1)
    {
      qarykabir="قاری کبیر";
    }
    else
    {
      qarykabir="Qari Kabeer";
    }
  }

  @override
  Widget build(BuildContext context)
  {

    return  Stack(
      children: [
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/page2_bg.jpg'),
              fit: BoxFit.fill,
            ),
          ),
        ),
        Container(
          width: double.infinity,
          height: 32.0,
          color: Color(0xBFfaf0d1),
          child: Align(
            child: Text(
              qarykabir,
              style: TextStyle(fontSize: 10.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false),
            ),
            alignment: AlignmentDirectional.center,
          ),
        ),
        Align(
          alignment: AlignmentDirectional.center,
          child: Container(margin: EdgeInsets.all(60.0),
            decoration: new BoxDecoration(
              color: Color(0xFFfdfaf0),
              shape: BoxShape.rectangle,
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
            ),
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: widget.item.length,
              itemBuilder: (context, position) {
                return Material(borderRadius:BorderRadius.all(Radius.circular(10.0)) ,color:Color(0xFFfdfaf0) ,
                  child: InkWell(onTap:()=>list_click(position) ,
                    child: Column(children: [
                      Align(
                        alignment: AlignmentDirectional.center,
                        child: Container(
                          alignment: AlignmentDirectional.center,
                          height: 100.0,
                          child: Text(
                            widget.item[position],
                            style: TextStyle(
                                fontSize: 20.0,
                                color: Color(0xFF72716c),
                                inherit: false,fontFamily: "grapic_ar"),
                          ),
                        ),
                      ),
                      Visibility(visible:(position!=widget.item.length-1) ,
                        child: Container(margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                          child: Divider(
                            height: 2.0,
                            color: Color(0xFF92582b),
                          ),
                        ),
                      ),
                    ]),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }
  void list_click(int posion)
  {//0=[qoran,maqamat,talavat,amuzesh]     1=[bayat,rast,nahavand,hejaz,segah,chahargah,saba]   2=[talavat1,talavat2]     3=[tajvid,tafkik,rukhani,ravankhani]    4=[namha,khatm,faliyat]
    print("posion=$posion");
    if(widget.page==0)
    {
      if(posion==0)
      {
        Navigator.push(
          context,
          //CupertinoPageRoute(builder: (context) => list_act()),
          MaterialPageRoute(builder: (context) => surelist_act(widget.db)),
        );
      }
      else if(posion==1)
      {
        var item;
        if(main_act.lang==1)
          item = ["مقام بیات", "مقام رست", "مقام نهاوند", "مقام حجاز","مقام سه گاه","مقام چهارگاه","مقام صبا"];
        else
          item = ["Bayat Tone", "Rast Tone", "Nahawand Tone", "Hejaz Tone","Saigah Tone","Chahargah Tone","Saba Tone"];
        Navigator.push(
          context,
          //CupertinoPageRoute(builder: (context) => list_act()),
          MaterialPageRoute(builder: (context) => list_act(item,widget.db,1)),
        );
      }
      else if(posion==2)
      {
        var item;
        if(main_act.lang==1)
          item = ["تلاوت اول","تلاوت دوم","تلاوت سوم","تلاوت چهارم","تلاوت پنجم","تلاوت ششم","تلاوت هفتم","تلاوت هشتم","تلاوت نهم","تلاوت دهم","تلاوت یازدهم","تلاوت دوازدهم","تلاوت سیزدهم","تلاوت چهاردهم","تلاوت پانزدهم","تلاوت شانزدهم","تلاوت هفدهم","تلاوت هجدهم","تلاوت نوزدهم","تلاوت بیستم"];
        else
          item = ["Recitation 1","Recitation 2","Recitation 3","Recitation 4","Recitation 5","Recitation 6","Recitation 7","Recitation 8","Recitation 9","Recitation 10","Recitation 11","Recitation 12","Recitation 13","Recitation 14","Recitation 15","Recitation 16","Recitation 17","Recitation 18","Recitation 19","Recitation 20"];
        Navigator.push(
          context,
          //CupertinoPageRoute(builder: (context) => list_act()),
          MaterialPageRoute(builder: (context) => list_act(item,widget.db,2)),
        );
      }
      else if(posion==3)
      {
        var item;
        if(main_act.lang==1)
          item = ["تجوید مقدماتی", "تفکیک خوانی", "روخوانی","روانخوانی"];
        else
          item = ["Primary Tajweed", "Tafkik", "Rokhani","Rawankhani"];
        Navigator.push(
          context,
          //CupertinoPageRoute(builder: (context) => list_act()),
          MaterialPageRoute(builder: (context) => list_act(item,widget.db,3)),
        );
      }
      else if(posion==4)//motefareqe
          {
        var item;
        if(main_act.lang==1)
          item = ["نام های خداوند","ختم نامه دعائیه","فعالیت های مدرسه","نهاد خیریه","دروس آنلاین"];
        else
          item = ["Names of Allah","Dua Khatam Al Quran","Our Activities","Charity Foundation","Online Lessons"];
        Navigator.push(
          context,
          //CupertinoPageRoute(builder: (context) => list_act()),
          MaterialPageRoute(builder: (context) => list_act(item,widget.db,4)),
        );
      }
      else if(posion==5)//ertebat bama
          {
        if(main_act.lang==1)
          showToast("به زودی این موارد اضافه میشود");
        else
          showToast("These items will be added soon");
      }
    }
    else if(widget.page==1)
    {
      String url="";
      if(posion==0)
      {
        url="https://gharikhabir.yousefi.org/telavat/Moqama-Bayat.mp4";
      }
      else if(posion==1)
      {
        url="https://gharikhabir.yousefi.org/telavat/Moqama-Rast.mp4";
      }
      else if(posion==2)
      {
        url="https://gharikhabir.yousefi.org/telavat/Moqama-Nahawand.mp4";
      }
      else if(posion==3)
      {
        url="https://gharikhabir.yousefi.org/telavat/Moqama-Hujaz.mp4";
      }
      else if(posion==4)
      {
        url="https://gharikhabir.yousefi.org/telavat/Moqama-Sega.mp4";
      }
      else if(posion==5)
      {
        url="https://gharikhabir.yousefi.org/telavat/Moqama-Charga.mp4";
      }
      else if(posion==6)
      {
        url="https://gharikhabir.yousefi.org/telavat/Moqama-Saba.mp4";
      }
      Navigator.push(context,
        MaterialPageRoute(builder: (context) => video_act(url)),
      );
    }
    else if(widget.page==2)//[talavat1...20]
        {
      String url="";
      if(posion==0)
      {
        url="https://m.youtube.com/watch?v=zyi12NnCdm4";
      }
      else if(posion==1)
      {
        url="https://www.youtube.com/watch?v=SctJFjlqswk&list=PLjwkMi0NCYvT-2hwaZKgc6Rek24UtgTTW&index=3";
      }
      else if(posion==2)
      {
        url="https://www.youtube.com/watch?v=P0Kg959EGbQ&list=PLjwkMi0NCYvT-2hwaZKgc6Rek24UtgTTW&index=12";
      }
      else if(posion==3)
      {
        url="https://www.youtube.com/watch?v=sCCL3nGXj-4&list=PLjwkMi0NCYvT-2hwaZKgc6Rek24UtgTTW&index=13";
      }
      else if(posion==4)
      {
        url="https://www.youtube.com/watch?v=fk0aM7waP_w&list=PLjwkMi0NCYvT-2hwaZKgc6Rek24UtgTTW&index=47";
      }
      else if(posion==5)
      {
        url="https://www.youtube.com/watch?v=gPneeS1DTDc&list=PLjwkMi0NCYvT-2hwaZKgc6Rek24UtgTTW&index=51";
      }
      else if(posion==6)
      {
        url="https://www.youtube.com/watch?v=rRn8wXrPp_g&list=PLjwkMi0NCYvT-2hwaZKgc6Rek24UtgTTW&index=61";
      }
      else if(posion==7)
      {
        url="https://www.youtube.com/watch?v=qodDusFI5qM&list=PLjwkMi0NCYvT-2hwaZKgc6Rek24UtgTTW&index=59";
      }
      else if(posion==8)
      {
        url="https://www.youtube.com/watch?v=2smRhlp08Gs&list=PLjwkMi0NCYvT-2hwaZKgc6Rek24UtgTTW&index=64";
      }
      else if(posion==9)
      {
        url="https://www.youtube.com/watch?v=IEe59rTxeOc&list=PLjwkMi0NCYvT-2hwaZKgc6Rek24UtgTTW&index=67";
      }
      else if(posion==10)
      {
        url="https://www.youtube.com/watch?v=0EZF8LvkfLs&list=PLjwkMi0NCYvT-2hwaZKgc6Rek24UtgTTW&index=16";
      }
      else if(posion==11)
      {
        url="https://www.youtube.com/watch?v=Y9AtkVb00a4";
      }
      else if(posion==12)
      {
        url="https://www.youtube.com/watch?v=bPn1KLzqU2c";
      }
      else if(posion==13)
      {
        url="https://www.youtube.com/watch?v=28gz3S5yfu0";
      }
      else if(posion==14)
      {
        url="https://www.youtube.com/watch?v=m8Ta7AtNIR0&t=333s";
      }
      else if(posion==15)
      {
        url="https://www.youtube.com/watch?v=hKPeJUJXYaA";
      }
      else if(posion==16)
      {
        url="https://www.youtube.com/watch?v=5d0Lq-1W2Bo";
      }
      else if(posion==17)
      {
        url="https://www.youtube.com/watch?v=oXsiMabyY6w";
      }
      else if(posion==18)
      {
        url="https://www.youtube.com/watch?v=weDQKn_JsJo";
      }
      else if(posion==19)
      {
        url="https://www.youtube.com/watch?v=yHzyl9ebR34";
      }

      launchURL(url);
    }
    else if(widget.page==3)//[tajvid,tafkik,rukhani,ravankhani]
        {
      String url="";
      if(posion==0)
      {
        if(main_act.lang==1)
          url="https://m.youtube.com/playlist?list=PLjwkMi0NCYvSLbdd-8-EH0hNPW3BGdToE";
        else
          url="https://m.youtube.com/playlist?list=PLjwkMi0NCYvRcy76yuCD1uxycsy1JCEkm";
        launchURL(url);
      }
      else if(posion==1)
      {
        if(main_act.lang==1)
          url="https://m.youtube.com/playlist?list=PLjwkMi0NCYvTdK_XmVePpCONgGKrSACr_";
        else
          showToast("These items will be added soon");
        launchURL(url);
      }
      else if(posion==2)
      {
        if(main_act.lang==1)
          url="https://m.youtube.com/playlist?list=PLjwkMi0NCYvT7BHBZsihQWrmewH14bwsS";
        else
          showToast("These items will be added soon");
        launchURL(url);
      }
      else if(posion==3)
      {
        if(main_act.lang==1)
          url="https://m.youtube.com/playlist?list=PLjwkMi0NCYvT0Pb5lbViXBcAQF1DzsJQR";
        else
          showToast("These items will be added soon");
        launchURL(url);
      }


    }
    else if(widget.page==4)//[namhayekhoda,khatm,faliyat madrese,kahad kheyriye,dorus online]
        {
      if(posion==0)
      {
        String url="https://s1.yousefi.ir/qarikabir/Asmaulla-01.mp4";
        Navigator.push(context,
          MaterialPageRoute(builder: (context) => video_act(url)),
        );
      }
      else if(posion==1)
      {
        String url="https://www.youtube.com/watch?v=ChkedIXdwjc&t=27s";
        launchURL(url);
      }
      else
      {
        if(main_act.lang==1)
          showToast("به زودی این موارد اضافه میشود");
        else
          showToast("These items will be added soon");
      }
    }

  }
  showToast(String str) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Color(0xFF000000),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: 12.0,
          ),
          Text(str,style: TextStyle(color: Colors.white,fontFamily: "grapic_ar"),),
        ],
      ),
    );


    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );

    // Custom Toast Position
    /*fToast.showToast(
        child: toast,
        toastDuration: Duration(seconds: 2),
        positionedToastBuilder: (context, child) {
          return Positioned(
            child: child,
            top: 16.0,
            left: 16.0,
          );
        });*/
  }
  launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}



