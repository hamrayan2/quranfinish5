import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qoranios/Activity/search_act.dart';
//import '../Widget/MyButton.dart';
import '../Classes/database_helper .dart';
import 'package:qoranios/Activity/list_act.dart';

class main_act extends StatefulWidget
{
  DictionaryDataBaseHelper db;
  static int lang=1;//1=fa   2=en   3=pash

  @override
  _main_actState createState() => _main_actState();
}

class _main_actState extends State<main_act>
{
  String qarykabir="";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    if(main_act.lang==1)
    {
      qarykabir="قاری کبیر";
    }
    else
    {
      qarykabir="Qari Kabeer";
    }
    widget.db=new DictionaryDataBaseHelper();
    widget.db.init();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: new ThemeData(canvasColor: Colors.black ),
      debugShowCheckedModeBanner: false,
      home: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('assets/images/page1_bg.jpg'),
                fit: BoxFit.fill,
              ),
            ),
          ),
          Container(
            width: double.infinity,
            height: 32.0,
            color: Color(0xBFfaf0d1),
            child: Align(
              child: Text(
                qarykabir,
                style: TextStyle(fontSize: 10.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false),
              ),
              alignment: AlignmentDirectional.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Align(alignment: AlignmentDirectional.bottomCenter,child: Wrap(spacing: 20.0,children: [Page1Btn("Eng",widget.db),Page1Btn("Fa",widget.db),Page1Btn("Pash",widget.db)],)),
          ),
        ],
      ),
    );
  }
}
class Page1Btn extends StatelessWidget
{
  DictionaryDataBaseHelper cur_db;
  String str="";
  Page1Btn(String str,DictionaryDataBaseHelper cur_db)
  {
    this.str=str;
    this.cur_db=cur_db;
  }
  var borderData=BorderSide(
    color: Color(0xFF92582b), //Color of the border
    style: BorderStyle.solid, //Style of the border
    width: 1, //width of the border
  );
  @override
  Widget build(BuildContext context) {
    return  DecoratedBox(
      decoration:
      ShapeDecoration(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)), color: Color(0xFFfdfaf0)),
      child: Theme(
        data: Theme.of(context).copyWith(
            buttonTheme: ButtonTheme.of(context).copyWith(
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap)),
        child: OutlineButton(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
          child: Text(str),
          onPressed: ()=>btn_click(context),
          borderSide: borderData,
        ),
      ),
    );
  }
  void btn_click(BuildContext context)
  {

    if(str.contains("Eng"))
    {
      var item = ["The Holy Quran", "Tones", "Popular Recitation", "Training", "Miscellaneous", "Contact us"];
      main_act.lang=2;
      Navigator.push(
        context,
        //CupertinoPageRoute(builder: (context) => list_act()),
        MaterialPageRoute(builder: (context) => list_act(item,cur_db,0)),
      );
    }
    else if(str.contains("Fa"))
    {
      var item = ["قرآن کریم", "مقامات", "تلاوت مجلسی", "آموزش", "متفرقه", "ارتباط با ما"];
      main_act.lang=1;
      Navigator.push(
        context,
        //CupertinoPageRoute(builder: (context) => list_act()),
        MaterialPageRoute(builder: (context) => list_act(item,cur_db,0)),
      );
    }
    else if(str.contains("Pash"))
    {
      var item = ["قرآن کریم", "مقامات", "مجلسي تلاوت", "زده کړه", "متفرقه", "له موږ سره اړیکه"];
      main_act.lang=3;
      Navigator.push(
        context,
        //CupertinoPageRoute(builder: (context) => list_act()),
        MaterialPageRoute(builder: (context) => list_act(item,cur_db,0)),
      );
    }
  }
  void _query() async
  {
    /*final dbHelper = DatabaseHelper.instance;
    final allRows = await dbHelper.queryAllRows();
    print('query all rows:');
    allRows.forEach(print);*/

    DictionaryDataBaseHelper db=new DictionaryDataBaseHelper();
    db.init();
  }
}

class MyTextBtn extends StatelessWidget
{
  String txt;
  int ind=0;//0=normal   1=bold
  int type=1;
  MyTextBtn(String txt,int ind,int type)
  {
    this.txt=txt;
    this.ind=ind;
    this.type=type;//1=sure   2=joz   3=manabe
  }
  @override
  Widget build(BuildContext context)
  {
    TextButton tb;
    if(ind==0)
    {
      tb=TextButton(onPressed: btn_click, child: Text(txt,style: TextStyle(color: Color(0xFFa0998f),fontWeight: FontWeight.normal),));
    }
    else if(ind==1)
    {
      tb=TextButton(onPressed: btn_click, child: Text(txt,style: TextStyle(color: Color(0xFFa0998f),fontWeight: FontWeight.bold),));
    }
    return tb;
  }
  void btn_click()
  {
    print(txt);

  }
}

