import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:just_audio/just_audio.dart';
import 'package:path_provider/path_provider.dart';
import 'package:qoranios/Activity/search_act.dart';
import 'package:qoranios/Activity/surelist_act.dart';
//import '../Widget/MyButton.dart';
import '../Classes/database_helper .dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
//import 'package:audioplayers/audioplayers.dart';
//import 'package:flutter_archive/flutter_archive.dart';
import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:qoranios/Activity/main_act.dart';


class matn3_act extends StatefulWidget {
  int type = 0; //1=sure   2=juz    3=manabe
  int index = 0;
  DictionaryDataBaseHelper db;
  List data = new List();

  matn3_act(int type, int index, DictionaryDataBaseHelper db) {
    this.type = type;
    this.index = index;
    this.db = db;
    print("type=$type    index=$index");

    //setData();
  }
  @override
  _matn3_actState createState() => _matn3_actState();
}

class _matn3_actState extends State<matn3_act>
{
  FToast fToast;
  BuildContext con;
  /////////////////////////////////////AudioPlayer audioPlayer = AudioPlayer();
  final player = AudioPlayer();
  /////////////////////////////////////////////
  var _positionSubscription;
  var position;
  String progress = '0';
  String downorext="دریافت";
  bool isplay=false;
  bool ispause=false;
  IconData play_ico=Icons.play_arrow;

  String url_tartil="https://s1.yousefi.ir/qarikabir/mobile/tartil.zip";
  String url_tahdir="https://s1.yousefi.ir/qarikabir/mobile/tahdir.zip";
  String url_tadvir="https://s1.yousefi.ir/qarikabir/mobile/tadvir.zip";
  String url_majlesi="https://s1.yousefi.ir/qarikabir/mobile/majlesi.zip";
  String url_farsidari="https://s1.yousefi.ir/qarikabir/mobile/farsidari.zip";
  String url_pashto="https://s1.yousefi.ir/qarikabir/mobile/pashto.zip";
  String url_english="https://s1.yousefi.ir/qarikabir/mobile/english.zip";

  Directory dir;
  String path;

  bool visible_top = true;
  bool visible_bot = true;
  bool visible_down=false;
  int _value = 0;//0=anva  1=tartil  2=tahdir   3=tadvir   4=majlesi   5=dari   6=pashto   7=en
  String sure_name = "";
  String sure_adr = "";
  List sure_data = new List();
  /*AudioPlayer player1;
  AudioPlayer player2;
  bool isplaying=false;
  bool isfirstplay=true;
  var cur_icon=Icons.play_arrow;*/
  bool besm_show = true;
  bool kadr_show=true;
  var icon_bookmark = Icons.bookmark_border;

  SharedPreferences prefs;

  String qarykabir="";
  String search="";
  String anva_qeraat="";
  String tartil_qeraat="";
  String tahdir_qeraat="";
  String tadvir_qeraat="";
  String dari_qeraat="";
  String pashtu_qeraat="";
  String english_qeraat="";

  String yes_down="";
  String no_down="";
  String matn_down="";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    fToast = FToast();
    fToast.init(context);
    if(main_act.lang==1)
    {
      qarykabir="قاری کبیر";
      search="جستجو";
      anva_qeraat="انواع قرائت";
      tartil_qeraat="قرائت ترتیل(استاد عبدالکبیر حیدری)";
      tahdir_qeraat="قرائت تحدیر(استاد عبدالکبیر حیدری)";
      tadvir_qeraat="قرائت تدویر(استاد عبدالکبیر حیدری)";
      dari_qeraat="ترجمه فارسی دری(استاد عبدالکبیر حیدری)";
      pashtu_qeraat="ترجمه پشتو(استاد عبدالکبیر حیدری)";
      english_qeraat="ترجمه انگلیسی(استاد عبدالکبیر حیدری)";

      yes_down="بله";
      no_down="خیر";
      matn_down="آیا مایل به دریافت صوت قرائت هستید؟";
    }
    else
    {
      qarykabir="Qari Kabeer";
      search="Search";
      anva_qeraat="Recitation Type";
      tartil_qeraat="Tarteel Recitation (Ustad Abdul Kabeer Haidari)";
      tahdir_qeraat="Tahdeer Recitation (Ustad Abdul Kabeer Haidari)";
      tadvir_qeraat="Tadweer Recitation (Ustad Abdul Kabeer Haidari)";
      dari_qeraat="Persian Translation (Ustad Abdul Kabeer Haidari)";
      pashtu_qeraat="Pashto Translation (Ustad Abdul Kabeer Haidari)";
      english_qeraat="English Translation (Ustad Abdul Kabeer Haidari)";

      yes_down="Yes";
      no_down="No";
      matn_down="Do You Want To Download Sound?";
    }
    /*_positionSubscription = audioPlayer.onAudioPositionChanged.listen(
            (p) => setState(() => position = p)
    );
    audioPlayer.onPlayerCompletion.listen((event) {
      setState(() {
        position = 0;
        play_ico=Icons.play_arrow;
        print("complete111");
      });
    });*////////////////////////////////////////////////////
    player.playerStateStream.listen((state)
    {
      //print("state=${state.processingState}");
      if(state.processingState==ProcessingState.completed)
      {
        setState(() {
          position = 0;
          play_ico=Icons.play_arrow;
          print("complete111");
        });
      }

    });
    player.positionStream.listen((pos) {
      position=pos;
    });
    ////////////////////////////////////////////////////////

    if (widget.index == 1 || widget.index == 9) {
      besm_show = false;
    } else {
      besm_show = true;
    }
    if(widget.type==2)
    {
      besm_show=false;
      kadr_show=false;
      visible_bot=false;
    }
    setData();
    setPath();
  }
  @override
  void dispose() async{
    super.dispose();
    print("dispose was called");
    ////////////////////////////////////////await audioPlayer.release();
    await player.dispose();
    ////////////////////////////////////////////////////////
    //print("type=${widget.type}   index=${widget.index}");
    await prefs.setInt('type', widget.type);
    await prefs.setInt('index', widget.index);
    //prefs.getInt("aaa")?? 0;
  }
  setData() async {
    prefs = await SharedPreferences.getInstance();
    if (widget.type == 1) {
      sure_data = await widget.db.getCurSure(widget.index) as List;
      //print("sure_data=${sure_data[0]['arabic_name']}");
      sure_name = "سوره ${sure_data[0]['arabic_name']}";
      if(main_act.lang==1)
        sure_adr = "صفحه ${sure_data[0]['start_page']}/جزء ${sure_data[0]['juz']}";
      else
        sure_adr = "Page ${sure_data[0]['start_page']}/Juz ${sure_data[0]['juz']}";
      widget.data = await widget.db.getSure(widget.index) as List;
      //print("bbbbb=$data");
      setState(() {});
      await Future.delayed(const Duration(seconds: 2), () {
        print("3333");
        visible_top=false;
        visible_bot=false;

        setState(() {});
      });
    } else if (widget.type == 2) {
      print("widget.index in setData()=${widget.index}");
      sure_data = await widget.db.getCurSure(widget.index) as List;
      //print("sure_data=${sure_data[0]['arabic_name']}");
      //sure_name = "جزء ${sure_data[0]['juz']}";
      sure_name = "جزء ${widget.index}";

      widget.data = await widget.db.getJuz(widget.index) as List;
      int num = widget.data.length;
      sure_adr = "تعداد آیات ${num}";
      //print("bbbbb=$data");
      setState(() {});
      await Future.delayed(const Duration(seconds: 3), () {
        print("3333");
        //visible_top = false;
        //visible_bot = false;
        //setState(() {});
      });
    }
  }
  setPath() async
  {
    dir = await getApplicationDocumentsDirectory();
    print("dir=$dir");//  /data/data/com.hamrayan.qoranios/app_flutter

    var isE=await Directory("${dir.path}/tartil").exists();
    if(!isE)
    {
      new Directory("${dir.path}/tartil").createSync();
    }
    isE=await Directory("${dir.path}/tahdir").exists();
    if(!isE)
    {
      new Directory("${dir.path}/tahdir").createSync();
    }
    isE=await Directory("${dir.path}/tadvir").exists();
    if(!isE)
    {
      new Directory("${dir.path}/tadvir").createSync();
    }
    isE=await Directory("${dir.path}/majlesi").exists();
    if(!isE)
    {
      new Directory("${dir.path}/majlesi").createSync();
    }
    isE=await Directory("${dir.path}/farsidari").exists();
    if(!isE)
    {
      new Directory("${dir.path}/farsidari").createSync();
    }
    isE=await Directory("${dir.path}/pashto").exists();
    if(!isE)
    {
      new Directory("${dir.path}/pashto").createSync();
    }
    isE=await Directory("${dir.path}/english").exists();
    if(!isE)
    {
      new Directory("${dir.path}/english").createSync();
    }
  }
  @override
  Widget build(BuildContext context)
  {
    //final audioPosition = Provider.of<Duration>(context);
    con=context;
    return  Directionality(
      textDirection: TextDirection.rtl,
      child: Stack(
        children: [
          Container(color: Color(0xFFfef8ea)), //fullpage
          Column(
            children: [
              Expanded(
                child: Container(color: Color(0xFFfaf0d1),
                  alignment: Alignment.center,
                  child: Text(qarykabir,
                    style: TextStyle(fontSize: 10.0, color: Colors.black, fontFamily: "grapic_ar", inherit: false),),
                ),
                flex: 1,
              ), //title
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    showPanel();
                  },
                  child: Container(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            margin:
                            EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 10.0),
                            child: Visibility(
                              visible: kadr_show,
                              child: Stack(
                                alignment: Alignment.center,
                                children: [
                                  Image.asset(
                                    "assets/images/kadr_name.jpg",
                                    fit: BoxFit.fill,
                                  ),
                                  Wrap(
                                    spacing: 5.0,
                                    children: [
                                      Image.asset(
                                        "assets/images/sure_ico.png",
                                        fit: BoxFit.fill,
                                        height: 25.0,
                                      ),
                                      Image.asset(
                                        "assets/images/sname_${widget.index}.png",
                                        fit: BoxFit.fill,
                                        height: 25.0,
                                      )
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          Visibility(
                              visible: besm_show,
                              child: Image.asset(
                                "assets/images/besm_img.png",
                                height: 35,
                              )),
                          Container(
                            margin: EdgeInsets.fromLTRB(5.0, 10.0, 5.0, 35.0),
                            child: RichText(
                              text: TextSpan(
                                style: TextStyle(fontFamily: "me_quran"),
                                children: getList(),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                flex: 20,
              ), //list
            ],
          ),
          Column(
            children: [
              Expanded(
                //hedear top button
                child: Material(
                  child: Visibility(
                      visible: visible_top,
                      child: getHeaderTop()
                  ),
                ),
                flex: 3,
              ), //
              Expanded(
                child: Visibility(
                    visible: false,
                    child: Container(color: Color(0xFFccccff))),
                flex: 25,
              ), //
              Expanded(
                //header bot
                child: Material(
                  child: Visibility(
                      visible: visible_bot,
                      child: Container(color: Color(0xFF645f5b),
                        child: Row(mainAxisSize: MainAxisSize.max,children:
                        [
                          Expanded(flex: 5,child: Container(color:Colors.white,child: Container(
                            height: double.infinity,
                            padding: EdgeInsets.fromLTRB(0.0, 0, 7.0, 0.0),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(0.0),
                                color: Color(0xFF645f5b),
                                border: null), //BoxDecoration
                            child: DropdownButtonHideUnderline(
                              child: Material(
                                color: Color(0xFF645f5b),
                                child: DropdownButton(isExpanded: true,
                                    value: _value,
                                    items: [
                                      DropdownMenuItem(
                                          child: Container(
                                              alignment: Alignment.centerRight,
                                              child: Text(
                                                  anva_qeraat,overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 11.0,
                                                      fontFamily: "grapic_ar"),
                                                  textAlign: TextAlign.right)),
                                          value: 0),
                                      DropdownMenuItem(
                                          child: Container(
                                              alignment: Alignment.centerRight,
                                              child: Text(
                                                  tartil_qeraat,overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 11.0,
                                                      fontFamily: "grapic_ar"),
                                                  textAlign: TextAlign.right)),
                                          value: 1),
                                      DropdownMenuItem(
                                          child: Container(
                                              alignment: Alignment.centerRight,
                                              child: Text(
                                                  tahdir_qeraat,overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 11.0,
                                                      fontFamily: "grapic_ar"),
                                                  textAlign: TextAlign.right)),
                                          value: 2),
                                      DropdownMenuItem(
                                          child: Container(
                                              alignment: Alignment.centerRight,
                                              child: Text(
                                                  tadvir_qeraat,overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 11.0,
                                                      fontFamily: "grapic_ar"),
                                                  textAlign: TextAlign.right)),
                                          value: 3),
                                      DropdownMenuItem(
                                          child: Container(
                                              alignment: Alignment.centerRight,
                                              child: Text(
                                                dari_qeraat,overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 10.0,
                                                    fontFamily: "grapic_ar"),
                                                textAlign: TextAlign.right,
                                              )),
                                          value: 5),
                                      DropdownMenuItem(
                                          child: Container(
                                              alignment: Alignment.centerRight,
                                              child: Text(
                                                pashtu_qeraat,overflow: TextOverflow.ellipsis,
                                                style: TextStyle(color: Colors.white, fontSize: 11.0, fontFamily: "grapic_ar"), textAlign: TextAlign.right,
                                              )),
                                          value: 6),
                                      DropdownMenuItem(
                                          child: Container(
                                              alignment: Alignment.centerRight,
                                              child: Text(
                                                  english_qeraat,overflow: TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 11.0,
                                                      fontFamily: "grapic_ar"),
                                                  textAlign: TextAlign.right)),
                                          value: 7),
                                    ],
                                    onChanged: (value) {
                                      setState(() {
                                        _value = value;
                                        print(_value);
                                      });
                                    }),
                              ), //DropdownButton
                            ), //DropdownButtonHideUnderline
                          ),)),
                          Expanded(flex: 4,child: Container(child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              InkWell(child: Icon(Icons.loop, color: Colors.white),onTap:()=>click_player(1)),
                              InkWell(child: Icon(Icons.fast_forward, color: Colors.white),onTap:()=>click_player(2)),
                              InkWell(child: Icon(play_ico, color: Colors.white),onTap:()=>click_player(3)),
                              InkWell(child: Icon(Icons.fast_rewind, color: Colors.white),onTap:()=>click_player(4)),
                              InkWell(child: Icon(Icons.stop, color: Colors.white),onTap:()=>click_player(5)),
                            ],
                          ),
                          )),
                        ]
                          ,),)),
                ),
                flex: 3,
              ), //
            ],
          ),
          Column(
            children: [
              Expanded(
                child: Visibility(
                    visible: false,
                    child: Container(color: Color(0xFFccccff))),
                flex: 3,
              ), //
              Expanded(
                child: Visibility(
                    visible: false,
                    child: Container(color: Color(0xFFccccff))),
                flex: 25,
              ), //
              Expanded(//down zip -extarct
                //header bot
                child: Material(
                  child: Visibility(
                      visible: visible_down,
                      child: Container(color: Color(0xFF645f5b),
                        child: Row(mainAxisSize: MainAxisSize.max,children:
                        [
                          //Expanded(flex: 20,child: Container(child: Text("دریافت ${progress}%",style: TextStyle(color: Colors.white)),)),
                          Expanded(flex: 20,child: Container(child: RichText(text: TextSpan(children:[TextSpan(text:downorext,style: TextStyle(fontSize: 12.0, color: Colors.white, fontFamily: "grapic_ar", inherit: false) ),TextSpan(text: "${progress}%",style: TextStyle(fontSize: 14.0, color: Colors.white, fontFamily: "nabi", inherit: false))]),),)),
                          Align(alignment: Alignment.centerLeft,child: Container(alignment: Alignment.centerLeft,child: CircularProgressIndicator(backgroundColor: Colors.white,),margin: EdgeInsets.all(5),)),
                        ]
                          ,),)),
                ),
                flex: 3,
              ), //
            ],
          )
        ],
      ),
    );
  }

  showPanel() {
    print("showPanel");
    if(visible_top==false)
    {
      setState(() {
        visible_top=true;
        if(widget.type==1)
          visible_bot=true;
      });
    }
    else
    {
      setState(() {
        visible_top=false;
        visible_bot=false;
      });
    }
  }

  List<TextSpan> getList() {
    List<TextSpan> spans = [];
    if (widget.index == 1) //hamd
        {
      for (int i = 0; i < widget.data.length; i++) {
        //print(widget.data[i]['text']);
        spans.add(TextSpan(
            text: widget.data[i]['text'] ,
            style: TextStyle(color: Colors.black, fontSize: 18)));
        spans.add(TextSpan(
            text: "﴿",
            style: TextStyle(fontFamily: "adobe",color: Colors.black, fontSize: 18)));
        spans.add(TextSpan(
            text: widget.data[i]['aya'].toString() ,
            style: TextStyle(fontFamily: "yekan",color: Colors.black, fontSize: 18)));
        spans.add(TextSpan(
            text: "﴾",
            style: TextStyle(fontFamily: "adobe",color: Colors.black, fontSize: 18)));
      }
    } else {
      for (int i = 0; i < widget.data.length; i++) {
        spans.add(TextSpan(text: widget.data[i]['text'] , style: TextStyle(color: Colors.black, fontSize: 18)));
        spans.add(TextSpan(text: "﴿" , style: TextStyle(fontFamily: "adobe",color: Colors.black, fontSize: 18)));
        spans.add(TextSpan(text: widget.data[i]['aya'].toString() , style: TextStyle(fontFamily: "yekan",color: Colors.black, fontSize: 18)));
        spans.add(TextSpan(text: "﴾" , style: TextStyle(fontFamily: "adobe",color: Colors.black, fontSize: 18)));
      }
    }

    return spans;
  }

  void nextSure() {
    if (widget.type == 1) {
      //cur_icon=Icons.play_arrow;
      widget.index++;
      if (widget.index > 114) {
        widget.index = 1;
      }
      widget.data = new List();
      setData();
    } else if (widget.type == 2) {
      //cur_icon=Icons.play_arrow;
      widget.index++;
      if (widget.index > 30) {
        widget.index = 1;
      }
      widget.data = new List();
      setData();
    }
  }

  void setBookmark() async {
    if (icon_bookmark == Icons.bookmark_border)
    {
      icon_bookmark = Icons.bookmark;

      await widget.db.updateDB(widget.index,widget.type,1);
    }
    else
    {
      icon_bookmark = Icons.bookmark_border;
      await widget.db.updateDB(widget.index,widget.type,0);
    }
  }
  Widget getHeaderTop()
  {
    return Container(
      color: Color(0xFF645f5b),
      child: Stack(
        children: [
          Container(),
          Align(
            alignment: Alignment.centerRight,
            child: Wrap(
              spacing: 10.0,
              children: [
                PopupMenuButton(
                  child: Icon(Icons.more_vert,
                      color: Colors.white),
                  itemBuilder: (context) => [
                    PopupMenuItem(
                      value: 1,
                      child: Text(search,textDirection: TextDirection.rtl,style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false)),
                    ),
                    /*PopupMenuItem(
                      value: 2,
                      child: Text("تنظیمات",textDirection: TextDirection.rtl,style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false)),
                    ),
                    PopupMenuItem(
                      value: 3,
                      child: Text("راهنما",textDirection: TextDirection.rtl,style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false)),
                    ),*/
                  ],
                  onSelected: (int index) {
                    print('index is $index');
                    if(index==1)
                    {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => search_act(widget.db)),
                      );
                    }
                  },

                ),
                InkWell(
                    child: Icon(icon_bookmark,
                        color: Colors.white),
                    onTap: () {
                      setBookmark();
                      setState(() {});
                    }),
              ],
            ),
          ),
          Padding(padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
            child: Align(alignment: Alignment.topCenter,child: Text(sure_name,style: TextStyle(fontSize: 14.0, color: Colors.white,fontFamily: "grapic_ar",inherit: false))),
          ),
          Padding(padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
            child: Align(alignment: Alignment.bottomCenter,child: Text(sure_adr,style: TextStyle(fontSize: 12.0, color: Colors.white,fontFamily: "grapic_ar",inherit: false))),
          ),
          Align(alignment: Alignment.center,child: InkWell(child: Container(margin: EdgeInsets.fromLTRB(130.0, 0.0, 0.0, 0.0),child: Icon(Icons.keyboard_arrow_right, color: Colors.white)),onTap: (){print("vvvvv");nextSure();},))
        ],
      ),
    );
  }
  void click_player(int n) async//1=loop  2=seekF   3=play   4=seekB   5=stop
      {
    String cur_url="";
    String cur_folder="";

    if(n==1)
    {
      ///////////////////////////////////////////audioPlayer.setReleaseMode(ReleaseMode.LOOP);
      await player.setLoopMode(LoopMode.one); // loop current item
      /////////////////////////////////////////////
      print("loop");
      print(isNetworkAvalable());
      bool ss=await isNetworkAvalable();
      print("ss=$ss");
    }
    if(n==2)
    {
      ////////////////////////////////////////////int result = await audioPlayer.seek(Duration(milliseconds: position.inMilliseconds + 5000));
      await player.seek(position+Duration(seconds: (7)));
      print("seek 10");
      /////////////////////////////////////////////
    }
    if(n==4)
    {
      ////////////////////////////////////////////int result = await audioPlayer.seek(Duration(milliseconds: position.inMilliseconds - 5000));
      await player.seek(position-Duration(seconds: (7)));
      ////////////////////////////////////////////
    }
    if(n==5)
    {
      isplay=false;
      ispause=false;
      play_ico=Icons.play_arrow;
      /////////////////////////////////////////////int result = await audioPlayer.stop();
      await player.stop();
      /////////////////////////////////////////////
      setState(() {});
    }
    if(n==3)
    {
      if(_value==0)
      {
        if(main_act.lang==1)
          showToast("ابتدا یک قرائت را از انواع قرائت انتخاب کنید");
        else
          showToast("First, select one of the reading types");
        return;
      }
      else if(_value==1)
      {
        cur_url="https://gharikhabir.yousefi.org/tartil/${widget.index}.mp3";
        cur_folder="tartil";
      }
      else if(_value==2)
      {
        cur_url="https://gharikhabir.yousefi.org/hadr/${widget.index}.mp3";
        cur_folder="tahdir";
      }
      else if(_value==3)
      {
        cur_url="https://gharikhabir.yousefi.org/tadvir/${widget.index}.mp3";
        cur_folder="tadvir";
      }
      else if(_value==4)
      {
        cur_url="https://gharikhabir.yousefi.org/majlesi/${widget.index}.mp3";
        cur_folder="majlesi";
      }
      else if(_value==5)
      {
        cur_url="https://gharikhabir.yousefi.org/dari/${widget.index}.mp3";
        cur_folder="farsidari";
      }
      else if(_value==6)
      {
        cur_url="https://gharikhabir.yousefi.org/pashto/${widget.index}.mp3";
        cur_folder="pashto";
      }
      else if(_value==7)
      {
        cur_url="https://gharikhabir.yousefi.org/english/${widget.index}.mp3";
        cur_folder="english";
      }
      //cur_url="http://www.everyayah.com/data/AbdulSamad_64kbps_QuranExplorer.Com/001001.mp3";
      //cur_folder="tartil";
      path="${dir.path}/$cur_folder";
      path="$path/${widget.index}.mp3";

      //path="${dir.path}/testsound2";
      print("path5=$path");
      print("cur_url=$cur_url");
      print("cur_folder=$cur_folder");
      //////////////////////////////////var isE=await Directory(path).exists();
      var isE=await File(path).exists();
      if(isE)
      {
        print("true");
        //path="$path/${widget.index}.mp3";
        print("ppp=$path");
        if(isplay )
        {
          isplay=false;
          ispause=true;
          play_ico=Icons.play_arrow;
          //////////////////////////////////////int result = await audioPlayer.pause();
          await player.pause();
          ////////////////////////////////////////////
        }
        else if(isplay==false && ispause==false)
        {
          isplay=true;
          play_ico=Icons.pause;
          /////////////////////////////////////int result = await audioPlayer.play(path, isLocal: true);
          var duration = await player.setFilePath(path);
          player.play();
          //////////////////////////////////////
        }
        else if(isplay==false && ispause==true)
        {
          isplay=true;
          play_ico=Icons.pause;
          /////////////////////////////////////////int result = await audioPlayer.resume();
          player.play();
          ////////////////////////////////////////
        }
        setState(() {});
      }
      else
      {
        print("false");
        //showAlertDialog(con,cur_url,cur_folder);
        showAlertDialog(con,cur_url,cur_folder);
      }

    }
  }
  showAlertDialog(BuildContext context,String cur_url,String cur_folder)
  {
    // set up the buttons
    Widget cancelButton = FlatButton(
      child: Text(no_down,style: TextStyle(fontSize: 12.0, color: Colors.black, fontFamily: "grapic_ar", inherit: false), textAlign: TextAlign.right),
      onPressed:  () {Navigator.of(context).pop();},
    );
    Widget continueButton = FlatButton(
      child: Text(yes_down,style: TextStyle(fontSize: 12.0, color: Colors.blue, fontFamily: "grapic_ar", inherit: false),),
      onPressed:  ()
      async {
        Navigator.of(context).pop();
        bool hasNet=await isNetworkAvalable();
        if(hasNet)
        {
          visible_down=true;
          downloadFile(cur_url,cur_folder);
        }
        else
        {
          if(main_act.lang==1)
            showAlertOne(context,"ابتدا به شبکه متصل شوید و دوباره تلاش کنید");
          else
            showAlertOne(context,"First Connect To Network");
        }

      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("",style: TextStyle(fontSize: 12.0, color: Colors.black, fontFamily: "grapic_ar", inherit: false), textAlign: TextAlign.right),
      content: Text(matn_down,style: TextStyle(fontSize: 12.0, color: Colors.black, fontFamily: "grapic_ar", inherit: false), textAlign: TextAlign.right),
      actions: [
        cancelButton,
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  Future<void> downloadFile(uri, folderName) async {
    /*setState(() {
      downloading = true;
    });*/

    //String savePath = await getFilePath(fileName);
    //print("savePath=$savePath");
    //////////////////////////////////String savePath = "${dir.path}/${folderName}.zip";
    String savePath = "${dir.path}/${folderName}/${widget.index}.mp3";
    print("savePath=$savePath");
    File f=File(savePath);
    Dio dio = Dio();

    int fileinserver=1;//0=not 1=yes
    try
    {

      dio.download(
        uri,
        savePath,
        onReceiveProgress: (rcv, total) {
          //print('received: ${rcv.toStringAsFixed(0)} out of total: ${total.toStringAsFixed(0)}');

          setState(() {
            progress = ((rcv / total) * 100).toStringAsFixed(0);
          });

          if (progress == '100')
          {
            setState(()
            {
              //isDownloaded = true;
              print("fffffffffffff");
              //fileinserver=1;

              ///////////////////////////////////////////////////final zipFile = File(savePath);
              final destinationDir = Directory("${dir.path}/${folderName}");
              print("destinationDir=$destinationDir");
              //final destinationDir = Directory("${dir.path}");
              try {
                /////////////////////////////////////////////////ZipFile.extractToDirectory(zipFile: zipFile, destinationDir: destinationDir);
                print("ccccccccccccccccccccccccc");
              } catch (e) {
                print("eee=$e");
              }
            });
          }
          else if (double.parse(progress) < 100) {}
        },
        deleteOnError: true,
      ).then((_) {
        setState(() {
          if (progress == '100')
          {
            //progress="بازگشایی";
            //print("gggggggggggggggg");
            visible_down=false;
            click_player(3);
          }

          //downloading = false;
        });
      });
    }
    catch(DioError)
    {
      print("sssssssssssssssssssssssss");
    }
    if(fileinserver==0)
    {
      showAlertOne(context, "برای این سوره، صوتی برای دریافت وجود ندارد");
      visible_down=false;
      setState(() {});
    }


  }
  isNetworkAvalable() async
  {
    try {
      final result = await InternetAddress.lookup('www.entekhab.ir');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        return true;
      }
    } on SocketException catch (_) {
      print('not connected');
      return false;
    }
  }
  sureHasSound() async
  {
    Dio dio2 = Dio();
    //final response1 = await dio2.download('https://www.google.com/');
  }
  showAlertOne(BuildContext context,String str)
  {

    Widget continueButton = TextButton(
      child: Center(child: Text("خب",style: TextStyle(fontSize: 12.0, color: Colors.blue, fontFamily: "grapic_ar", inherit: false),)),
      onPressed:  ()
      {
        Navigator.of(context).pop();
        //visible_down=true;
        //downloadFile(cur_url,cur_folder);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text(" ",style: TextStyle(fontSize: 12.0, color: Colors.black, fontFamily: "grapic_ar", inherit: false), textAlign: TextAlign.right),
      content: Text(str,style: TextStyle(fontSize: 12.0, color: Colors.black, fontFamily: "grapic_ar", inherit: false), textAlign: TextAlign.right),
      actions: [
        continueButton,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
  showToast(String str) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Color(0xFF000000),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: 12.0,
          ),
          Text(str,style: TextStyle(color: Colors.white,fontFamily: "grapic_ar"),),
        ],
      ),
    );


    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 2),
    );

    // Custom Toast Position
    /*fToast.showToast(
        child: toast,
        toastDuration: Duration(seconds: 2),
        positionedToastBuilder: (context, child) {
          return Positioned(
            child: child,
            top: 16.0,
            left: 16.0,
          );
        });*/
  }
}
