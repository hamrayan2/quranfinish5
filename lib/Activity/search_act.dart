import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import '../Widget/MyButton.dart';
import '../Classes/database_helper .dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'matn3_act.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:qoranios/Activity/main_act.dart';
class search_act extends StatefulWidget
{
  DictionaryDataBaseHelper db;
  search_act(DictionaryDataBaseHelper db)
  {
    this.db=db;
  }
  @override
  _search_actState createState() => _search_actState();
}

class _search_actState extends State<search_act>
{
  TextEditingController mycontroller = TextEditingController();
  List data=new List();
  String qarykabir="";
  @override
  void initState()
  {
    // TODO: implement initState
    super.initState() ;
    SystemChrome.setEnabledSystemUIOverlays([]);
    if(main_act.lang==1)
    {
      qarykabir="قاری کبیر";
    }
    else if(main_act.lang==2)
    {
      qarykabir="Qari Kabeer";
    }
    else
    {
      qarykabir="قاري کبیر";
    }
  }
  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            color: Color(0xFFf7f0e6),
          ),
          Container(
            width: double.infinity,
            height: 32.0,
            color: Color(0xBFfaf0d1),
            child: Align(
              child: Text(
                qarykabir,
                style: TextStyle(fontSize: 10.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false),
              ),
              alignment: AlignmentDirectional.center,
            ),
          ),
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.fromLTRB(15, 40, 15, 0),
                child: Material(
                  child: Container(
                    color: Color(0xFFf7f0e6),
                    child: Container(
                      //alignment: Alignment.center,
                      height: 35.0,
                      decoration: new BoxDecoration(
                          color: Color(0xFFe8e1d9),
                          border: new Border.all(
                              color: Color(0xFFe8e1d9), width: 1.0),
                          borderRadius: new BorderRadius.circular(13.0)),
                      child: new TextFormField(
                        controller: mycontroller,
                        style: TextStyle(fontSize: 12.0, color: Colors.black,fontFamily: "grapic_ar"),
                        textAlign: TextAlign.right,
                        //textAlignVertical: TextAlignVertical.bottom,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.zero,
                          prefixIcon: IconButton(icon: Icon(Icons.arrow_back_ios),onPressed:(){
                            mycontroller.clear();
                            },),//suffixIcon
                          suffixIcon: IconButton(icon: Icon(Icons.search_outlined),onPressed:searchDb,),//prefixIcon
                          border: InputBorder.none,
                          focusedBorder: InputBorder.none,
                          enabledBorder: InputBorder.none,
                          errorBorder: InputBorder.none,
                          disabledBorder: InputBorder.none,
                          //contentPadding: EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                        ),
                        onChanged: (text)
                        {
                          print(text);
                        },

                      ),
                    ),
                  ),
                ),
              ),

              Expanded(
                child: Container(
                  margin: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 10.0),
                  decoration: new BoxDecoration(
                    color: Color(0xFFfdfaf0),
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                  child: getList(),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
  Widget getList()
  {
    if(data.length!=0)
    {
      return ListView.builder(
        itemCount: data.length,
        itemBuilder: (context, position) {
          return Material(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            color: Color(0xFFfdfaf0),
            child: InkWell(
              onTap: () {
                print(position);
                print(data[position]["sura"]);
                Navigator.push(
                  context,
                  //CupertinoPageRoute(builder: (context) => list_act()),
                  MaterialPageRoute(builder: (context) => matn3_act(1,data[position]["sura"],widget.db)),
                );
              },
              child: Column(children: [
                Align(
                  alignment: AlignmentDirectional.center,
                  child: Container(
                    alignment: AlignmentDirectional.center,
                    height: 60.0,
                    child: Container(
                      margin: EdgeInsets.fromLTRB(40.0, 0.0, 30.0, 0.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          RichText(text: TextSpan(children: [TextSpan(text:"${position+1}. ", style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "nabi",inherit: false)),TextSpan(text: "سوره ${data[position]["arabic_name"]}",style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false))]),),
                          RichText(text: TextSpan(children: [TextSpan(text:"آیه ", style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false)),TextSpan(text: "${data[position]['aya']}",style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "nabi",inherit: false))]),),
                          //Text("${position+1}.سوره ${data[position]["arabic_name"]}",textDirection: TextDirection.rtl,style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false)),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                  child: Divider(
                    height: 2.0,
                    color: Color(0xFF92582b),
                  ),
                ),
              ]),
            ),
          );
        },
      );
    }
    else
    {
      return null;
    }

  }
  searchDb() async
  {
    print(mycontroller.text);
    data=new List();
    data=await widget.db.getSearch(mycontroller.text) as List   ;
    print("data.length=${data.length}");
    //print(data[0]);
    setState(() {});
  }

}

