
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import '../Widget/MyButton.dart';
import '../Classes/database_helper .dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

import 'matn3_act.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:qoranios/Activity/main_act.dart';

class surelist_act extends StatefulWidget
{
  DictionaryDataBaseHelper db;
  surelist_act(DictionaryDataBaseHelper db)
  {
    this.db=db;
  }
  @override
  _surelist_actState createState() => _surelist_actState();

}

class _surelist_actState extends State<surelist_act>
{
  SharedPreferences prefs;
  List data=new List();


  int cur_ind = 1; //1=sure   2=joz   3=manabe
  int firstrun=0;
  TextEditingController mycontroller = TextEditingController();

  int last_index=0;
  int last_type=0;
  String last_name="";
  int last_page=0;
  int last_juz=0;


  String qarykabir="";
  String sure_header="";
  String juz_header="";
  String manabe_header="";
  @override
  void initState() {
    // TODO: implement initState
    super.initState() ;
    SystemChrome.setEnabledSystemUIOverlays([]);
    //widget.db=new DictionaryDataBaseHelper();
    //widget.db.init();
    if(main_act.lang==1)
    {
      qarykabir="قاری کبیر";
      sure_header="سوره";
      juz_header="جزء";
      manabe_header="منابع";
    }
    else if(main_act.lang==2)
    {
      qarykabir="Qari Kabeer";
      sure_header="Surah";
      juz_header="Juz";
      manabe_header="Sources";
    }
    else
      {
        qarykabir="قاري کبیر";
        sure_header="سوره";
        juz_header="سپاره";
        manabe_header="پاڼه";
      }

    setPrefs();
    btn_click("سوره");
  }
  setPrefs() async
  {
    prefs = await SharedPreferences.getInstance();
    last_index=prefs.getInt("index")?? 0;
    last_type=prefs.getInt("type")?? 0;
    List cursure=await widget.db.getCurSure(last_index) as List ;
    print("last_index=$last_index   lastseen=$cursure");
    //print(cursure[0]["name"]);
    last_name=cursure[0]["name"]+"";
    last_page=cursure[0]["start_page"];
    last_juz=cursure[0]["juz"];
    //print(cursure);
    setState(() {});
  }
  @override
  Widget build(BuildContext context)
  {
    int btn1 = 0;
    int btn2 = 0;
    int btn3 = 0;
    if (cur_ind == 1) {
      btn1 = 1;
    } else if (cur_ind == 2) {
      btn2 = 1;
    } else if (cur_ind == 3) {
      btn3 = 1;
    }
    return  Directionality(
        textDirection: TextDirection.rtl,
        child: Stack(
          children: [
            Container(
              width: double.infinity,
              height: double.infinity,
              color: Color(0xFFf7f0e6),
            ),
            Container(
              width: double.infinity,
              height: 32.0,
              color: Color(0xBFfaf0d1),
              child: Align(
                child: Text(
                  qarykabir,
                  style: TextStyle(fontSize: 10.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false),
                ),
                alignment: AlignmentDirectional.center,
              ),
            ),
            Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(15, 40, 15, 0),
                  child: Material(
                    child: Container(
                      color: Color(0xFFf7f0e6),
                      child: Container(
                        //alignment: Alignment.center,
                        height: 35.0,
                        decoration: new BoxDecoration(
                            color: Color(0xFFe8e1d9),
                            border: new Border.all(
                                color: Color(0xFFe8e1d9), width: 1.0),
                            borderRadius: new BorderRadius.circular(13.0)),
                        child: new TextFormField(
                          controller: mycontroller,
                          style: TextStyle(fontSize: 12.0, color: Colors.black,fontFamily: "grapic_ar"),
                          textAlign: TextAlign.right,
                          //textAlignVertical: TextAlignVertical.bottom,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.zero,
                            prefixIcon: IconButton(alignment: AlignmentDirectional.center,icon: Icon(Icons.arrow_back_ios),onPressed:(){
                              mycontroller.clear();
                              if(cur_ind==1)
                                btn_click("سوره");
                              else if(cur_ind==2)
                                btn_click("جزء");},),//suffixIcon
                            suffixIcon: IconButton(alignment: Alignment.centerLeft,icon: Icon(Icons.search_outlined),onPressed:searchDb,),//prefixIcon
                            border: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            errorBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            //contentPadding: EdgeInsets.only(left: 15, bottom: 11, top: 11, right: 15),
                          ),
                          onChanged: (text)
                          {
                            if(cur_ind==1)
                              {
                                btn_click("سوره");
                              }
                            else if(cur_ind==2)
                              {
                                btn_click("جزء");
                              }

                            else if(cur_ind==3)
                              {
                                btn_click("منابع");
                              }
                            print("dddddddd=");
                            print(text);
                          },

                        ),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    getTextBtn(sure_header, btn1),
                    getTextBtn(juz_header, btn2),
                    getTextBtn(manabe_header, btn3),
                  ],
                ),
                Expanded(
                  child: Container(
                    margin: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 10.0),
                    decoration: new BoxDecoration(
                      color: Color(0xFFfdfaf0),
                      shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                    child: getList(),
                  ),
                ),
              ],
            )
          ],
        ),
      );
  }

  TextButton getTextBtn(String txt, int ind)
  {
    TextButton tb;
    if (ind == 0) {
      tb = TextButton(
          onPressed: () => btn_click(txt),
          child: Text(
            txt,
            style: TextStyle(
                color: Color(0xFFa0998f), fontWeight: FontWeight.normal,fontFamily: "grapic_ar",inherit: false,fontSize: 12.0),
          ));
    } else if (ind == 1) {
      tb = TextButton(
          onPressed: () => btn_click(txt),
          child: Text(
            txt,
            style: TextStyle(
                color: Color(0xFFa0998f), fontWeight: FontWeight.bold,fontFamily: "grapic_ar",inherit: false,fontSize: 12.0),
          ));
    }
    /*if(firstrun==0)
      {
        firstrun=1;
        btn_click("سوره");
      }*/
    return tb;
  }//getTextBtn

  Future<void> btn_click(String txt) async
  {
    print("txt=$txt");
    if (txt.contains("منابع")||txt.contains("Sources")||txt.contains("پاڼه"))
    {
      cur_ind = 3;

      List data1=new List();
      List data2=new List();
      data1=await widget.db.getFav("sura") as List ;
      data2=await widget.db.getFav("juz_page") as List ;

      print("data1.lenght=${data1.length}   data2.lenght=${data2.length} ");
      print("data1=$data1");
      print("**************************************");
      print("data2=$data2");
      data=new List();
      for(int n=0;n<data1.length;n++)
        {
          data.add(data1[n]);
        }
      for(int n=0;n<data2.length;n++)
      {
        //data.add(data2[n]);
      }
      print("**************************************");
      print("data=$data");
      //data=await widget.db.getAll2("sura",mycontroller.text,1) as List ;
//      print("bbbbb=$res");
//      print("##############################");
//      print(res[0]["name"]);
//      print("res.length=${res.length}");
    }
    if (txt.contains("جزء")||txt.contains("Juz")||txt.contains("سپاره"))
    {
      cur_ind = 2;
      data=new List();
      data=await widget.db.getAll2("juz_page",mycontroller.text,cur_ind) as List   ;
    }
    if (txt.contains("سوره")||txt.contains("Surah"))
    {
      cur_ind = 1;
      data=new List();
      data=await widget.db.getAll2("sura",mycontroller.text,cur_ind) as List   ;
    }
    print(txt);
    setState(() {});
  }//btn_click
  //****************************************************************//

  Widget getList()
  {
    print("rebuild");
    if(data.length!=0)
    {
      print("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
      print(data[0]["name"]);

      if(cur_ind==1)//sure
          {
        return ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, position) {
            return Material(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              color: Color(0xFFfdfaf0),
              child: InkWell(
                onTap: () {
                  print(position);
                  Navigator.push(
                    context,
                    //CupertinoPageRoute(builder: (context) => list_act()),
                    MaterialPageRoute(builder: (context) => matn3_act(cur_ind,data[position]["id"],widget.db)),
                  );
                },
                child: Column(children: [
                  Align(
                    alignment: AlignmentDirectional.center,
                    child: Container(
                      alignment: AlignmentDirectional.center,
                      height: 60.0,
                      child: Container(
                        margin: EdgeInsets.fromLTRB(40.0, 0.0, 30.0, 0.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            RichText(text: TextSpan(children: [TextSpan(text:"${data[position]["id"]}. ", style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "yekan",inherit: false)),TextSpan(text: "سوره ${data[position]["arabic_name"]}",style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false))]),),
                            Text("${data[position]['start_page']}",style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "yekan",inherit: false)),
                            //Text("${position+1}.سوره ${data[position]["arabic_name"]}",textDirection: TextDirection.rtl,style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false)),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                    child: Divider(
                      height: 2.0,
                      color: Color(0xFF92582b),
                    ),
                  ),
                ]),
              ),
            );
          },
        );
      }
      else if(cur_ind==2)//juz
          {
        return ListView.builder(
          itemCount: data.length,
          itemBuilder: (context, position) {
            return Material(
              borderRadius: BorderRadius.all(Radius.circular(10.0)),
              color: Color(0xFFfdfaf0),
              child: InkWell(
                onTap: () {
                  print(position);
                  Navigator.push(context,
                    //CupertinoPageRoute(builder: (context) => list_act()),
                    MaterialPageRoute(builder: (context) => matn3_act(cur_ind,data[position]["id"],widget.db)),
                  );
                },
                child: Column(children: [
                  Align(
                    alignment: AlignmentDirectional.center,
                    child: Container(
                      alignment: AlignmentDirectional.center,
                      height: 60.0,
                      child: Container(
                        margin: EdgeInsets.fromLTRB(40.0, 0.0, 30.0, 0.0),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            //Text("جزء ${position+1}",textDirection: TextDirection.rtl),
                            RichText(text: TextSpan(children: [TextSpan(text: "جزء ",style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "grapic_ar",inherit: false)),TextSpan(text: "${data[position]["id"]}",style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "yekan",inherit: false))]),),
                            Text("${data[position]['page']}",style: TextStyle(fontSize: 14.0, color: Colors.black,fontFamily: "yekan",inherit: false)),
                          ],
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
                    child: Divider(
                      height: 2.0,
                      color: Color(0xFF92582b),
                    ),
                  ),
                ]),
              ),
            );
          },
        );
      }
      else if(cur_ind==3)//manabe
          {
            print("sssssssssssss");
            setPrefs();
        return Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded
              (flex: 3,child: Container(padding: EdgeInsets.fromLTRB(0, 0, 35, 0),alignment: Alignment.centerRight,width: double.infinity,child: Text("آخرین صفحه",style: TextStyle(fontSize: 18.0, color: Colors.white, fontFamily: "grapic_ar", inherit: false)),decoration: new BoxDecoration(
                color: Color(0xFFbbb8b1), borderRadius: new BorderRadius.only(topRight: Radius.circular(13.0),topLeft:Radius.circular(13.0) ,bottomLeft:Radius.circular(0.0) ,bottomRight:Radius.circular(0.0))),)
              ),
            Expanded(flex: 5,child: Material(
              child: InkWell(onTap: (){
                print("22222222222222222");
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) => matn3_act(1,last_index,widget.db)),
                );},
                child: Container(color: Color(0xFFfdfaf0),child: Stack(children: [
                  Container(),
                  Container(alignment: Alignment.centerRight,child: Image.asset("assets/images/manabe_ico.png",height: 30,),margin: EdgeInsets.fromLTRB(0, 0, 20, 0),),
                  Container(alignment: Alignment.topRight,child: Text(last_name,style:TextStyle(fontSize: 20.0, color: Color(0xFF9d9892), fontFamily: "grapic_ar", inherit: false)),margin: EdgeInsets.fromLTRB(0, 18, 70, 0)),
                  Container(alignment: Alignment.bottomRight,child: Text("صفحه ${last_page}/ جژ ${last_juz}",style:TextStyle(fontSize: 12.0, color: Color(0xFF9d9892), fontFamily: "grapic_ar", inherit: false)),margin: EdgeInsets.fromLTRB(0, 0, 70, 18)),
                  Container(alignment: Alignment.centerLeft,child: Text("$last_page",style:TextStyle(fontSize: 20.0, color: Color(0xFF9d9892), fontFamily: "yekan", inherit: false)),margin: EdgeInsets.fromLTRB(30, 0, 0, 0)),
                ],),),
              ),
            )),
            Expanded
              (flex: 3,child: Container(padding: EdgeInsets.fromLTRB(0, 0, 35, 0),alignment: Alignment.centerRight,width: double.infinity,child: Text("منابع صفحات",style: TextStyle(fontSize: 18.0, color: Colors.white, fontFamily: "grapic_ar", inherit: false)),decoration: new BoxDecoration(
                color: Color(0xFFbbb8b1), borderRadius: new BorderRadius.only(topRight: Radius.circular(0.0),topLeft:Radius.circular(0.0) ,bottomLeft:Radius.circular(0.0) ,bottomRight:Radius.circular(0.0))),)
            ),
            Expanded(flex: 20,child: Container(color: Color(0xFFfdfaf0),child: ListView.builder(padding: EdgeInsets.all(0.0),
                itemCount: data.length,
                itemBuilder: (context, position)
                {
                  return Container(
                    child: Material(
                      child: InkWell(onTap: (){
                        //print("pos=$position   ${data[position]['id']}");
                        Navigator.push(context,
                          MaterialPageRoute(builder: (context) => matn3_act(1,data[position]['id'],widget.db)),
                        );},
                        child: Container(height: 80,color: Color(0xFFfdfaf0),
                          child: Stack(children: [
                            Container(),
                            Container(alignment: Alignment.centerRight,child: Image.asset("assets/images/manabe_ico.png",height: 30,),margin: EdgeInsets.fromLTRB(0, 0, 20, 0),),
                            Container(alignment: Alignment.topRight,child: Text(data[position]['name'],style:TextStyle(fontSize: 20.0, color: Color(0xFF9d9892), fontFamily: "grapic_ar", inherit: false)),margin: EdgeInsets.fromLTRB(0, 18, 70, 0)),
                            Container(alignment: Alignment.bottomRight,child: Text("صفحه ${data[position]['start_page']}/ جژ ${data[position]['juz']}",style:TextStyle(fontSize: 12.0, color: Color(0xFF9d9892), fontFamily: "grapic_ar", inherit: false)),margin: EdgeInsets.fromLTRB(0, 0, 70, 18)),
                            Container(alignment: Alignment.centerLeft,child: Text("${data[position]['start_page']}",style:TextStyle(fontSize: 20.0, color: Color(0xFF9d9892), fontFamily: "nabi", inherit: false)),margin: EdgeInsets.fromLTRB(30, 0, 0, 0)),
                          ],),
                        ),
                      ),
                    ),
                  );
                }

            ),)),
          ],
        );
      }
    }
    else
    {
      return null;
    }

    /*var res=db.getAll2("sura")   ;
    print("aaaa=$res");
    print(res);*/
    //var res= db.getAll("sura")  ;
    //print(res[0]["name"]);
    //print(res(0)("name"));
    //Future<List>  res= db.getAll("sura") ;


  }//getList()
  void searchDb()
  {
    //print(mycontroller.text);

  }
}
