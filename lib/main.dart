import 'dart:io';

import 'package:flutter/material.dart';
import 'package:qoranios/Activity/main_act.dart';
import 'Activity/video_act.dart';

class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}
void main()
{
  HttpOverrides.global = new MyHttpOverrides();
  runApp(main_act());
  //runApp( splash_act());
  //runApp(MaterialApp(home: search_act(),));
  //runApp(video_act());
}
